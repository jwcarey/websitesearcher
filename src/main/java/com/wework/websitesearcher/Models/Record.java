package com.wework.websitesearcher.Models;

/**
 * POJO represeting a single line of urls.txt
 */
public class Record {
    private final int rank;
    private final String url;
    private final int linkingRootDomains;
    private final int externalLinks;
    private final double mozRank;
    private final double mozTrust;
    private ResultType resultType;

    /**
     * Basic constructor.
     *
     * @param rank
     * @param url
     * @param linkingRootDomains
     * @param externalLinks
     * @param mozRank
     * @param mozTrust
     */
    public Record(int rank, String url, int linkingRootDomains, int externalLinks, double mozRank, double mozTrust){
        this.rank = rank;
        this.url = url;
        this.linkingRootDomains = linkingRootDomains;
        this.externalLinks = externalLinks;
        this.mozRank = mozRank;
        this.mozTrust = mozTrust;
    }

    public String getUrl() { return url; }

    public ResultType getResultType(){ return this.resultType; }

    public int getRank() {
        return rank;
    }

    public int getLinkingRootDomains() {
        return linkingRootDomains;
    }

    public int getExternalLinks() {
        return externalLinks;
    }

    public double getMozRank() {
        return mozRank;
    }

    public double getMozTrust() {
        return mozTrust;
    }

    public void setResultType(ResultType resultType){ this.resultType = resultType; }



}
