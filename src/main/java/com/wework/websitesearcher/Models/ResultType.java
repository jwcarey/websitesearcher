package com.wework.websitesearcher.Models;

public enum ResultType{
    Found, NotFound, ConnectionError, Timeout
}

