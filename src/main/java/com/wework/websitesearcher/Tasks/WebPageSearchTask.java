package com.wework.websitesearcher.Tasks;

import com.wework.websitesearcher.Models.Record;
import com.wework.websitesearcher.Models.ResultType;
import org.jsoup.Jsoup;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.concurrent.Callable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Searches web page for string - currently hardcoded to be google.com -
 */
public class WebPageSearchTask implements Callable<Record> {
    private final Record record;
    private static final String REGEX_STRING = ".*google.com.*";
    private static final int TIMEOUT_IN_MILLIS = 5 * 1000;
    private static final String PROTOCOL = "http://www.";

    public WebPageSearchTask(Record record){
        this.record = record;
    }

    @Override
    public Record call(){
        String html;
        try{
            html = Jsoup.connect(PROTOCOL + record.getUrl()).timeout(TIMEOUT_IN_MILLIS).get().html();
        }
        catch(SocketTimeoutException e){
            record.setResultType(ResultType.Timeout);
            return record;
        }
        catch(IOException e){
            record.setResultType(ResultType.ConnectionError);
            return record;
        }
        Pattern regex = Pattern.compile(REGEX_STRING, Pattern.CASE_INSENSITIVE);
        Matcher matcher = regex.matcher(html);
        if(matcher.find()){
            record.setResultType(ResultType.Found);
        }
        else{
            record.setResultType(ResultType.NotFound);
        }
        return record;
    }
}
