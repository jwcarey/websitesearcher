package com.wework.websitesearcher.Services;

import com.wework.websitesearcher.Models.Record;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class ResultFileWriter {
    private static FileWriter writer;

    public static void writeResultsToFile(List<Future<Record>> results) throws ExecutionException, InterruptedException, IOException{
        writer = new FileWriter("results.txt");
        writeHeader();
        for(Future<Record> result : results){
            writeLine(result.get());
        }
    }

    private static void writeHeader() throws IOException{
        writer.write("Rank");
        writer.write(",");
        writer.write("URL");
        writer.write(",");
        writer.write("Linking Root Domains");
        writer.write(",");
        writer.write("External Links");
        writer.write(",");
        writer.write("mozRank");
        writer.write(",");
        writer.write("mozTrust");
        writer.write(",");
        writer.write("results");
        writer.write("\n");
    }

    private static void writeLine(Record record) throws IOException{
        writer.write(String.valueOf(record.getRank()));
        writer.write(",");
        writer.write("\""+ record.getUrl() +"\"");
        writer.write(",");
        writer.write(String.valueOf(record.getLinkingRootDomains()));
        writer.write(",");
        writer.write(String.valueOf(record.getExternalLinks()));
        writer.write(",");
        writer.write(String.valueOf(record.getMozRank()));
        writer.write(",");
        writer.write(String.valueOf(record.getMozTrust()));
        writer.write(",");
        writer.write(record.getResultType().toString());
        writer.write("\n");
    }
}
