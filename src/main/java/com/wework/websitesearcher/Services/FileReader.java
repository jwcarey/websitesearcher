package com.wework.websitesearcher.Services;

import com.wework.websitesearcher.Models.Record;
import com.wework.websitesearcher.Tasks.WebPageSearchTask;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Reads CSV and builds Queue of tasks.
 */
public class FileReader {
    public static Queue<WebPageSearchTask> getTasksFromCSV() throws IOException{
        Queue<WebPageSearchTask> tasks = new LinkedBlockingQueue<>();
        File csvFile = new ClassPathResource("urls.txt").getFile();
        CSVParser parser = CSVParser.parse(csvFile, Charset.defaultCharset(),  CSVFormat.DEFAULT);
        boolean skipFirst = false;
        for(CSVRecord csvRecord : parser){
            if(skipFirst){
                int rank = Integer.parseInt(csvRecord.get(0));
                String url = csvRecord.get(1);
                int linkingRootDomain = Integer.parseInt(csvRecord.get(2));
                int externalLinks = Integer.parseInt(csvRecord.get(3));
                double mozRank = Double.parseDouble(csvRecord.get(4));
                double mozTrust = Double.parseDouble(csvRecord.get(5));
                Record recordObject = new Record(rank, url, linkingRootDomain, externalLinks, mozRank, mozTrust);
                tasks.add(new WebPageSearchTask(recordObject));
            }
            skipFirst = true;
        }
        return tasks;
    }
}
