package com.wework.websitesearcher.Services;

import com.wework.websitesearcher.Models.Record;
import com.wework.websitesearcher.Tasks.WebPageSearchTask;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.FutureTask;

public class WebpageSearchTaskManager {

    private int maxThreadCount;
    private Set<FutureTask<Record>> processingResults = new HashSet<>();

    /**
     * Manages the submission and connection limits for WebPageSearchTasks
     *
     * @param maxThreadCount the maximum concurrent WebPageSearchTasks
     */
    public WebpageSearchTaskManager(int maxThreadCount){
        this.maxThreadCount = maxThreadCount;
    }

    public FutureTask<Record> submit(WebPageSearchTask task) throws InterruptedException{
        waitForCapacity();
        FutureTask<Record> futureTask = new FutureTask<>(task);
        Thread t = new Thread(futureTask);
        t.start();
        processingResults.add(futureTask);
        return futureTask;
    }

    private void waitForCapacity() throws InterruptedException{
        while(processingResults.size()>= maxThreadCount){
            Set<FutureTask<Record>> newlyFinishedResults = new HashSet<>();
            //Sleeps for a second to spare CPU
            Thread.sleep(1000);
            for(FutureTask<Record> processingResult : processingResults){
                if(processingResult.isDone()){
                    newlyFinishedResults.add(processingResult);
                }
            }
            for(FutureTask<Record> result : newlyFinishedResults){
                processingResults.remove(result);
            }
        }
    }






}
