package com.wework.websitesearcher;

import com.wework.websitesearcher.Models.Record;
import com.wework.websitesearcher.Services.FileReader;
import com.wework.websitesearcher.Services.ResultFileWriter;
import com.wework.websitesearcher.Tasks.WebPageSearchTask;
import com.wework.websitesearcher.Services.WebpageSearchTaskManager;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.*;

@SpringBootApplication
@EnableAutoConfiguration
public class CodingSampleApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(CodingSampleApplication.class, args);
	}

	@Override
	public void run(String... args) throws MalformedURLException, IOException, InterruptedException, ExecutionException {
		final Queue<WebPageSearchTask> tasks;
		final List<Future<Record>> results = new ArrayList<>();
		tasks = FileReader.getTasksFromCSV();
		WebpageSearchTaskManager pool = new WebpageSearchTaskManager(20);
		for(WebPageSearchTask task : tasks){
			results.add(pool.submit(task));
		}
		ResultFileWriter.writeResultsToFile(results);
	}
}
