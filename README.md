# README #

Adding case insensitivy to the REGEX was the last thing I did and it ended up being much more expensive than I assumed it would. This implementation isn't very quick because of that. I decided to take the 
"this part isn't too important" bit from the code assignment to heart I suppose.

If the constraints didn't exist, I would probobally have implemented concurrency through some sort of ExecutorService. The solution that's there probobally isn't perfect in a real world situation when compared to the libraries that exist.

Theres not much in the form of comments and no tests here. I had some -end of summer- time constraints on this guy.

I assume whoever is going to run this through some IDE, so at the moment the urls.txt its reading from is in the resources folder. The results.txt it produces should be in the root of the project folder.

All settings to speak of are hardcoded. D: